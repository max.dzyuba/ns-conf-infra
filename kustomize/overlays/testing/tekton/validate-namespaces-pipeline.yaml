apiVersion: tekton.dev/v1beta1
kind: Pipeline
metadata:
  name: validate-namespaces
spec:
  workspaces:
  - name: shared-workspace
  params:
  - name: git-url
    type: string
    description: url of the git repo for the code
  - name: gitlab-repository-full-name
    type: string
    description: |
      The GitLab repository full name, e.g.: arbetsformedlingen/devops/aardvark-demo
  - name: git-commit
    description: sha of commit to deploy
  - name: gitlab-project
    description: ID of GitLab project.
  - name: ocs-ui-base-url
    type: string
    description: Base URL to OCS admin interface.
    default: "https://console-openshift-console.test.services.jtech.se"
  tasks:
  - name: gitlab-run-status
    taskRef:
      name: jobtech-gitlab-set-status
      kind: ClusterTask
    params:
    - name: REPO_FULL_NAME
      value: $(params.gitlab-repository-full-name)
    - name: GITLAB_TOKEN_SECRET_NAME
      value: gitlab-access
    - name: GITLAB_TOKEN_SECRET_KEY
      value: username
    - name: SHA
      value: $(params.git-commit)
    - name: TARGET_URL
      value: "$(params.ocs-ui-base-url)/k8s/ns/$(context.pipelineRun.namespace)/tekton.dev~v1beta1~PipelineRun/$(context.pipelineRun.name)"
    - name: CONTEXT
      value: "ci/ns-config"
    - name: STATE
      value: running
    - name: DESCRIPTION
      value: Check of infra started.
  - name: fetch-repository
    taskRef:
      name: git-clone
      kind: ClusterTask
    workspaces:
    - name: output
      workspace: shared-workspace
    params:
    - name: url
      value: $(params.git-url)
    - name: subdirectory
      value: "source"
    - name: deleteExisting
      value: "true"
    - name: revision
      value: $(params.git-commit)
    runAfter:
    - gitlab-run-status
  - name: validate
    taskRef:
      name: ns-conf
      kind: Task
    workspaces:
    - name: source
      workspace: shared-workspace
    params:
    - name: SCRIPT
      value: |
          ok="true"
          for i in kustomize/overlays/*; do
            e=$(echo $i | cut -d/ -f3)
            if [ $e != "develop" ] ; then
              echo $i
              echo \#\# $e >> $(workspaces.source.path)/output.txt
              echo >> $(workspaces.source.path)/output.txt
              if ! ns-conf validate --input_file=$i/namespaces.yaml >> $(workspaces.source.path)/output.txt 2>&1 ; then
                ok="false"
                echo Cannot validate namespaces for $e. >> $(workspaces.source.path)/output.txt
              fi
              echo >> $(workspaces.source.path)/output.txt
            fi
          done
          if [ ${ok} = "false" ] ; then
            exit 1
          fi
    runAfter:
    - fetch-repository
  finally:
  - name: post-validation-info
    taskRef:
      name: post-review-comment
      kind: Task
    workspaces:
    - name: source
      workspace: shared-workspace
    params:
    - name: review-file
      value: output.txt
    - name: commit-sha
      value: "$(params.git-commit)"
    - name: project
      value: "$(params.gitlab-project)"
  - name: gitlab-approve
    taskRef:
      name: gitlab-approve-commit
      kind: Task
    when:
    - input: "$(tasks.status)"
      operator: in
      values:
      - "Succeeded"
      - "Completed"
    params:
    - name: REPO_FULL_NAME
      value: $(params.gitlab-repository-full-name)
    - name: GITLAB_TOKEN_SECRET_NAME
      value: gitlab-access
    - name: GITLAB_TOKEN_SECRET_KEY
      value: username
    - name: SHA
      value: $(params.git-commit)
  - name: gitlab-success-status
    taskRef:
      name: jobtech-gitlab-set-status
      kind: ClusterTask
    when:
    - input: "$(tasks.status)"
      operator: in
      values:
      - "Succeeded"
      - "Completed"
    params:
    - name: REPO_FULL_NAME
      value: $(params.gitlab-repository-full-name)
    - name: GITLAB_TOKEN_SECRET_NAME
      value: gitlab-access
    - name: GITLAB_TOKEN_SECRET_KEY
      value: username
    - name: SHA
      value: $(params.git-commit)
    - name: TARGET_URL
      value: "$(params.ocs-ui-base-url)/k8s/ns/$(context.pipelineRun.namespace)/tekton.dev~v1beta1~PipelineRun/$(context.pipelineRun.name)"
    - name: CONTEXT
      value: "ci/ns-config"
    - name: STATE
      value: success
    - name: DESCRIPTION
      value: Check of infrastructure code passed.
  - name: gitlab-failed-status
    taskRef:
      name: jobtech-gitlab-set-status
      kind: ClusterTask
    when:
    - input: "$(tasks.status)"
      operator: in
      values:
      - "Failed"
      - "None"
    params:
    - name: REPO_FULL_NAME
      value: $(params.gitlab-repository-full-name)
    - name: GITLAB_TOKEN_SECRET_NAME
      value: gitlab-access
    - name: GITLAB_TOKEN_SECRET_KEY
      value: username
    - name: SHA
      value: $(params.git-commit)
    - name: TARGET_URL
      value: "$(params.ocs-ui-base-url)/k8s/ns/$(context.pipelineRun.namespace)/tekton.dev~v1beta1~PipelineRun/$(context.pipelineRun.name)"
    - name: CONTEXT
      value: "ci/ns-config"
    - name: STATE
      value: failed
    - name: DESCRIPTION
      value: Check of infrastructure code passed. See logs for pipeline.
