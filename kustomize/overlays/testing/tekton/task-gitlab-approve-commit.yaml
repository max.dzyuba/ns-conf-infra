apiVersion: tekton.dev/v1beta1
kind: Task
metadata:
  name: gitlab-approve-commit
spec:
  description: >-
    Approve all Merge Requests connected to a commit SHA.
  params:
    - name: GITLAB_HOST_URL
      description: |
        The GitLab host, adjust this if you run a GitLab enterprise.
      default: "gitlab.com"
      type: string
    - name: API_PATH_PREFIX
      description: |
        The API path prefix, GitLab Enterprise has a prefix e.g. /api/v4
      default: "/api/v4"
      type: string
    - name: REPO_FULL_NAME
      description: |
        The GitLab repository full name, e.g.: tektoncd/catalog
      type: string
    - name: GITLAB_TOKEN_SECRET_NAME
      description: |
        The name of the kubernetes secret that contains the GitLab token, default: gitlab-api-secret
      type: string
      default: gitlab-api-secret
    - name: GITLAB_TOKEN_SECRET_KEY
      description: |
        The key within the kubernetes secret that contains the GitLab token, default: token
      type: string
      default: token
    - name: SHA
      description: |
        Commit SHA to set the status for.
      type: string
  steps:
    - name: approve-commit
      image: registry.access.redhat.com/ubi8/python-38@sha256:af6f93b81f9313de95966e8cd681edb9dbcb5fdbddc5a4cc365af8e4534096ef
      env:
        - name: GITLAB_TOKEN
          valueFrom:
            secretKeyRef:
              name: $(params.GITLAB_TOKEN_SECRET_NAME)
              key: $(params.GITLAB_TOKEN_SECRET_KEY)
        - name: HOME
          value: /tekton/home
      script: |
        #!/usr/libexec/platform-python

        import os
        import time
        import json
        import http.client
        import urllib.parse

        GITLAB_TOKEN = os.getenv("GITLAB_TOKEN")
        GITLAB_HOST_URL = "$(params.GITLAB_HOST_URL)"
        API_PATH_PREFIX = "$(params.API_PATH_PREFIX)"
        REPO_FULL_NAME = "$(params.REPO_FULL_NAME)"
        SHA = "$(params.SHA)"

        HEADERS = {
            "User-Agent": "TektonCD, the peaceful cat",
            "Authorization": f"Bearer {GITLAB_TOKEN}",
        }


        def _get_gitlab_connection():
            if GITLAB_HOST_URL.startswith("http://"):
                conn = http.client.HTTPConnection(GITLAB_HOST_URL[7:])
            elif GITLAB_HOST_URL.startswith("https://"):
                conn = http.client.HTTPSConnection(GITLAB_HOST_URL[8:])
            else:
                conn = http.client.HTTPSConnection(GITLAB_HOST_URL)
            return conn


        def _url_encode(data):
            return urllib.parse.quote(data, safe="")


        def get_merge_requests(repo_name, sha):
            api_url = f"{API_PATH_PREFIX}/projects/{_url_encode(repo_name)}/repository/commits/{sha}/merge_requests"
            conn = _get_gitlab_connection()
            try:
                conn.request("GET", api_url, headers=HEADERS)
                resp = conn.getresponse()
                response_data = json.loads(resp.read())
                mr_iids = map(lambda i: i['iid'], response_data)
            finally:
                conn.close()
            return mr_iids


        def approve_merge_request(repo_name, mr_iid):
            api_url = f"{API_PATH_PREFIX}/projects/{_url_encode(repo_name)}/merge_requests/{mr_iid}/approve"
            conn = _get_gitlab_connection()
            try:
                conn.request("POST", api_url, headers=HEADERS)
            finally:
                conn.close()


        mr_list = get_merge_requests(REPO_FULL_NAME, SHA)
        sleep = 60
        while not mr_list: # Retry and wait for a wile
            time.sleep(sleep)
            sleep *= 2
            mr_list = get_merge_requests(REPO_FULL_NAME, SHA)
            if sleep > 240:
                break

        for iid in mr_list:
            approve_merge_request(REPO_FULL_NAME, iid)
